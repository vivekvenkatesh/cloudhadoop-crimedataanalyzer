package crimedata;

        
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.CombineFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class CrimeAnalyzer {

	public static void main(String[] args) throws Exception {
		if(args.length == 3) {
			Configuration conf = new Configuration();  
			if(args[2].matches("^\\d+$")) {
				// Check if the argument entered is a number
				int temp = Integer.parseInt(args[2]);
				if(temp >=1 && temp <=5) {
					conf.set("regiondef", args[2]);
					Job job = new Job(conf, "regioncrime");
					job.setOutputKeyClass(Text.class);
					job.setOutputValueClass(Text.class);
					job.setJarByClass(CrimeAnalyzer.class);
					job.setMapperClass(RegionCrime.Map1.class);
					job.setReducerClass(RegionCrime.Reduce.class);
					job.setNumReduceTasks(1);
					FileInputFormat.addInputPath(job, new Path(args[0]));
					//job.setInputFormatClass(TextInputFormat.class);
					job.setInputFormatClass(CFInputFormat.class);
					job.setOutputFormatClass(TextOutputFormat.class);
					
					
					FileOutputFormat.setOutputPath(job, new Path(args[1]));

					job.waitForCompletion(true); 
				}
				else {
					System.out.println("Specify the number of characters (in the range 1-5) to use in the coordinates for region definition.");
				}
			}
			else {
				System.out.println("Please enter a valid Region Definition. Specify the number of characters (in the range 1-5) to use in the coordinates for region definition.");
			}
		}
		else {
			System.out.println("Expecting 3 arguments: <input path><output path><region definition (1-5)>");
		}

	}

}
