package crimedata;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;


public class RegionCrime {
	///public static class Map1 extends Mapper<LongWritable, Text, Text, Text> {
	public static class Map1 extends Mapper<FileLineWritable, Text, Text, Text> {
		/*
		 * Mapper 1 emits 'region' as key and 'crime type' as value
		 * Region is defined by (East, North) coordinates and we use only 1st digit to represent the coordinates
		 * Crime Type includes Anti-social behavior, burglary etc.,
		 */
	    private Text region;
	    private Text crimeValue;
	    
	    ///public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
	    public void map(FileLineWritable key, Text value, Context context) throws IOException, InterruptedException {
	    	// Split the first line that has headings
	    	String line = value.toString();
	    	///if(key.get() > 0) {
	    	if(!line.startsWith("Crime")) {
	    		// Split the crime record by commas
	    		String[] crimeRecord = line.split(",");
	    		// crimeRecord[0].startsWith("Crime") == false to skip the heading in the first line
	    		if(crimeRecord.length >= 8) {

	    			// Only if the record has 9 fields it is considered to be a valid data
	    			// Fields 4 and 5 correspond to Easting and Northing respectively
	    			// Field 7 corresponds to Crime Type
	    			
	    			// Make Sure that the length of Easting and Northing is 4 (As there are few records with no location) 
	    			// There are certain records with lengths 4,5, 6
	    			if(crimeRecord[4].length() >= 4 && crimeRecord[5].length() >= 4) {	
	    				Configuration conf = context.getConfiguration();
	    				int regionDef = Integer.parseInt(conf.get("regiondef")); 
	    				String easting = crimeRecord[4].substring(0, regionDef);
	    				String northing = crimeRecord[5].substring(0, regionDef);
	    				region = new Text("(" + easting + "," + northing + ")");
	    			}
	    			else {
	    				region  = new Text("(Unknown Location)");
	    			}
	    			String crimeType = crimeRecord[7];
	    			crimeValue = new Text(crimeType);
	    			context.write (region, crimeValue);
	    			
	    		}	
	    	}
	    }
	 } // End of Map1 
	
	public static class Reduce extends Reducer<Text, Text, Text, IntWritable> {
		/*
		 * Use a HashMap with key as composite
		 * (1,1,Crime_Name) 
		 * and value represents the count of each crime. This will work as the same region goes to the same reducer
		 */
		
		Map<Text, Integer> H = new HashMap<Text, Integer>();
		
		Text composite_key;
		public void reduce(Text key, Iterable<Text> values, Context context) 
				throws IOException, InterruptedException {
			for(Text val : values) {
				String tempKey = key.toString();
				String tempValue = val.toString();
				
				composite_key = new Text(tempKey.concat(tempValue));
				if(H.get(composite_key) == null) {
					H.put(composite_key, 1);
				}
				else {
					H.put(composite_key, H.get(composite_key) + 1);
				}
			}
			
			// Iterate through the HashMap and write the output
			Iterator<Map.Entry<Text, Integer>> entries = H.entrySet().iterator();
	        while(entries.hasNext()) {
	            Map.Entry<Text, Integer> entry = entries.next();
	            context.write(entry.getKey(), new IntWritable(entry.getValue()));
	       
	        }
	        // We need to do this. Else duplicate records will be created
			H.clear(); 		
		}
	} // End of Reduce
	        
}
